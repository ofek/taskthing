module.exports = {
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000'
  },
  head: {
    title: 'starter',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  css: [
    'purecss/build/base.css',
    'purecss/build/forms.css',
    'purecss/build/buttons.css',
    '~assets/css/main.css'
  ],
  build: {
    vendor: ['gsap', 'vuex-class', 'nuxt-class-component']
  },
  modules: ['~modules/typescript.ts']
}
