import { Task } from './Task'

export interface TaskOverview {
    current: Task | null;
    today: Task[];
    week: Task[];
    month: Task[];
    whenever: Task[];
}