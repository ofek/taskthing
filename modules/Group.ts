export class Group {
    id: number;
    name: string;
    abbrv: string;
}