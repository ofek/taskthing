interface Label {
    name: string;
    color: string;
}

export class Task {
    id: number;
    title: string;
    labels: Label[];
    groupId: number;
}